FROM openjdk:8-alpine
COPY target/eureka*.jar eureka.jar
#EXPOSE 8761
CMD ["java", "-jar", "eureka.jar"]
